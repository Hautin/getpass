<?php 
require_once("includes/functions.php"); 
$user = detect_session(1); // tester si l'utilisateur est identifié 
require_once("includes/header.php");

if(isset($_POST ['registration'])){
	if(!empty($_POST['mail1']) && !empty($_POST['mail2']) && !empty($_POST['pass1']) && !empty($_POST['pass2'])) {
		require_once("includes/config.php");
		require_once("includes/database.php");
		$mail1 = htmlspecialchars($_POST['mail1']);
		$mail2 = htmlspecialchars($_POST['mail2']);
		$mdp1 =  sha1($_POST['pass1']);
		$mdp2 =  sha1($_POST['pass2']);
		$nusers= newutilisateur($mail1, $mail2, $mdp1, $mdp2);
		
	} else{
		echo "Tout les champs doivent être compléter";
		}
}
?>

<div class="tbl-reg">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post'>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
				<p class="inp-reg">Adresse e-mail :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="email" name="mail1" maxlength="250">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-8 col-lg-3 offset-lg-2 offset-md-2">
				<p class="inp-reg">Confirmez votre adresse e-mail :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="email" name="mail2" maxlength="250">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
				<p class="inp-reg">Mot de passe :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="password" name="pass1" maxlength="64">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-8 col-lg-3 offset-lg-2 offset-md-2">
				<p class="inp-reg">Confirmez votre mot de passe :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="password" name="pass2" maxlength="64">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-1 col-md-1 col-lg-1 offset-lg-5 offset-md-5 offset-sm-1">
				<input class="inp-sub" type="submit" name="registration" value="S'inscrire!">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-11 col-md-8 col-lg-4 offset-lgS-5 offset-md-3 offset-sm-1">
				<?php
				if(isset($_POST ['registration'])){
					echo $nusers;
				}
				?>
			</div>
		</div>
	</form>
</div>

<?php require_once("includes/footer.php"); ?>
