<?php 
require_once("includes/functions.php"); 
$user = detect_session(); // tester si l'utilisateur est identifié 
require_once("includes/header.php");
require_once("includes/config.php");
include("includes/database.php");	

$bdd= bddconnect();
$reponsetbl = $bdd->prepare("SELECT * FROM types WHERE id_members='".$_SESSION['user']."'");
$reponsetbl->execute();
$donneestbl= $reponsetbl->fetchAll(PDO::FETCH_OBJ);
$selectnweb=1;
// 
$erreur=null;
if(isset($_POST['genok']) && isset($_POST['nbcar']) && isset($_POST['nvsec']) && !empty($_POST['nvsec']) && !empty($_POST['nbcar'])){
	$nbcar=$_POST['nbcar'];
	$nvsec=$_POST['nvsec'];
	$mdpgen=generate($nbcar, $nvsec);
	
}
if(isset($_POST ['new'])){
	$selectnweb=htmlspecialchars($_POST['selectnweb']);
	$nnewtbl = htmlspecialchars($_POST['nomnewtbl']);
	$nom = htmlspecialchars($_POST['nomenr']);
	$login = htmlspecialchars($_POST['login']);
	$mdp1 =  htmlspecialchars($_POST['pass1']);
	$mdp2 =  htmlspecialchars($_POST['pass2']);

	if(!empty($_POST['selectnweb']) && $_POST['selectnweb']!=1){ // si il n'est pas vide et qu'il n'est pas = à 0, on ne créer pas un nouvelle "table"
		//var_dump($selectnweb); test de la valeur du select
		if(!empty($nom) && !empty($login) && !empty($mdp1) && !empty($mdp2)) { // condition pour éxécuter la fonnction
			$ok= newstock($selectnweb, $nom, $login, $mdp1, $mdp2);
			
		} else{
			$erreur= "Vous devez au minimum compléter le nom d'enregistrement, le login et le mots de passe ainsi que ca confirmation";
		}
	} elseif (!empty($_POST['nomnewtbl'])){ // sinonou on créer une nouvelle table
		if(!empty($nom) && !empty($login) && !empty($mdp1) && !empty($mdp2)) { // condition pour éxécuter la fonnction
			$newtableok=ntable($nnewtbl); // le résultat de la fonction et dans la variable qui l'appel
			$selectnweb=$newtableok;
			$ok= newstock($selectnweb, $nom, $login, $mdp1, $mdp2);
		} else{
			$erreur= "Vous devez au minimum compléter le nom d'enregistrement, le login et le mots de passe ainsi que ca confirmation";
		}
	} else {
		$erreur= "Sélectionnez une table ou créer en une nouvelle!";
	}
}
?>
<script>

function toggleDiv(divId) {
	$("#"+divId).toggle();
	//$("#imgp").attr("src","css/images/moin.jpg");
}

</script>
<div class="tbl-reg">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post'>
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
				<p class="inp-reg">Choisissez le type :</p>
			</div>
			<div class="col-sm-3 col-md-3 col-lg-4">
				<select class="slecttbl" name="selectnweb" id="web" data-target="nsiteweb" data-url="lweb.php">
					<option value="1">Sélectionner un type</option>
                    <option value="2">Email</option>
                    <option value="3">Boutique en ligne</option>
                    <option value="4">Réseaux sociaux</option>
                    <?php foreach ($donneestbl as $nweb):?>
                        <option value="<?php echo $nweb->id_types;?>"<?php echo $selectnweb == $nweb->id_types ? ' selected' : ''; ?>><?php echo $nweb->name_types;?></option> <!--condition facultative-->
                    <?php endforeach; ?>
				</select><a href="javascript:toggleDiv('nouvtbl');" class="newtbl">Nouveau Types</a>
			</div>
		</div>
		<div id="nouvtbl">
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
					<p class="inp-reg">Nom du nouveau type :</p>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-2">
					<input type="text" name="nomnewtbl" maxlength="250">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
				<p class="inp-reg">Nom d'enregistrement :</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-2">
				<input type="text" name="nomenr" maxlength="250">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-1 offset-lg-4 offset-md-1 offset-sm-1">
				<p class="inp-reg">Login :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-4">
				<input type="text" name="login" maxlength="250">  <a href="javascript:toggleDiv('gen8'); javascript:toggleDiv('gen16'); javascript:toggleDiv('gen32'); javascript:toggleDiv('gen64'); javascript:toggleDiv('nvsec1'); javascript:toggleDiv('nvsec2'); javascript:toggleDiv('nvsec3'); javascript:toggleDiv('genok');" class="newtbl">Générer</a> <!-- a voir ou mettre le générateur--> 
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
				<p class="inp-reg">Mot de passe :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="text" name="pass1" maxlength="64" value="<?php if(isset($mdpgen)):echo $mdpgen; endif;?>">
			</div>
			<div id="gen8"class="col-sm-1 col-md-1 col-lg-1">
				<p>8</p><input type="radio" value="8" name="nbcar">
			</div>
			<div id="gen16"class="col-sm-1 col-md-1 col-lg-1">
				<p>16</p><input type="radio" checked="checked" value="16" name="nbcar">
			</div>
			<div id="gen32"class="col-sm-1 col-md-1 col-lg-1">
				<p>32</p><input type="radio" value="32" name="nbcar">
			</div>
			<div id="gen64"class="col-sm-1 col-md-1 col-lg-1">
				<p>64</p><input type="radio" value="64" name="nbcar">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
				<p class="inp-reg">Confirmez le mot de passe :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="text" name="pass2" maxlength="64" value="<?php if(isset($mdpgen)):echo $mdpgen; endif;?>">
			</div>
			<div id="nvsec1"class="col-sm-1 col-md-1 col-lg-1">
				<p>faible</p><input type="radio" value="1" name="nvsec">
			</div>
			<div id="nvsec2"class="col-sm-1 col-md-1 col-lg-1">
				<p>moyen</p><input type="radio" value="2" name="nvsec">
			</div>
			<div id="nvsec3"class="col-sm-1 col-md-1 col-lg-1">
				<p>fort</p><input type="radio" checked="checked" value="3" name="nvsec">
			</div>
			<div id="genok"class="col-sm-1 col-md-1 col-lg-">
				<input type="submit" value="Ok générer!" name="genok">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-1 col-md-1 col-lg-1 offset-lg-5 offset-md-5 offset-sm-1">
				<input class="inp-sub" type="submit" name="new" value="Enregistrez!">
			</div>
		</div>
	</form>
	<div class="row">
			<div class="col-sm-7 col-md-7 col-lg-4 offset-lg-5 offset-md-5 offset-sm-1">
				<?php
				if(isset($_POST ['new'])){
					echo $erreur;
				}
				?>
			</div>
		</div>
</div>

<?php require_once("includes/footer.php"); ?>
