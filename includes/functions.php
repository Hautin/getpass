<?php 
// tester si l'utilisateur est identifié
// 0 = n'est pas connecter & 1 = est connecter
function detect_session($choice = 0) { 
	session_start();
	if(isset($_SESSION['user'])) { // connecté ?
        if($choice == 1) {
			$usercon=$_SESSION['user'];
    		header('Location: index.php');
	    	exit;
    	}
    }
    else {
    	if($choice == 0) {
    		header('Location: login.php');
	    	exit;
    	}
    }
}

// déconnexion de l'utilisateur
function disconnect_session() { 
	session_start();
	$_SESSION = array();
	session_destroy();
	header('Location: login.php');
	exit;
	
}

// générateur de mots de passe
function generate($nbcar, $nvsec){
	$return="";
	if($nvsec==1){ // faible caractere min + mag
		$caract = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
	} elseif ($nvsec==2){ // élever caractere min + mag + chiffre
		$caract = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789";
	} elseif ($nvsec==3){ // dure caractere min + mag + chiffre + symbole
		$caract = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789@!=+-/*&#";
	}

	

	for($i=1; $i <= $nbcar; $i++){
		// On compte le nombre de caractères
		$Nbr = strlen($caract);
		// On choisit un caractère au hasard dans la chaine sélectionnée :
		$Nbr = mt_rand(0,($Nbr-1));
		
		// Pour finir, on écrit le résultat :
		$return .= $caract[$Nbr];
	}

	return $return;
}
?>