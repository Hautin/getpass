<!doctype html>
<?php

?>
<html lang="fr">
<head>
  <meta charset="utf-8">
  
  <!-- <script src="script.js"></script> -->
  <link rel="stylesheet" href="css/style.css">
  <link href="bootstrap/css/freelancer.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  <script src="bootstrap/vendor/jquery/jquery.js"></script>
  <script src="bootstrap/vendor/tether.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

</head>

<body>
<?php
if(isset($_SESSION['user'])) { // connecté ?
?>
<div class="deco">
  <div class="row">
      <div class="verind col-sm-2 col-md-2 col-lg-1 offset-md-8 offset-lg-6">
        <a href="index.php" id="elein">index</a>
      </div>
      <div class="verger col-sm-2 col-md-2 col-lg-2">
        <a href="types.php" id="eleg">gérer les types</a>
      </div>
      <div class="col-sm-2 col-md-2 col-lg-1">
        <form action="includes/deconexion.php" methode="POST">
          <input type="submit" class="newdeco" name"deco" value="Déconexion">
        </form>
      </div>
  </div>
</div>

<?php

}
?>
<div class="title-header">
  <div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-lg-3">
      <h1 id="thead">Getpass</h1>
    </div>
  </div>
</div>