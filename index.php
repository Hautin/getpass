<?php require_once("includes/functions.php");
$user = detect_session(); // tester si l'utilisateur est identifié ?
require_once("includes/config.php");
include("includes/database.php");
require_once("includes/header.php");
//$selectnweb=2;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="js/jquery.chained.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
// script liste lié
$(document).ready(function() {
    $("#nsiteweb").hide();
    $('#selectnweb').change(function(){ 
        $(function(){
            $("#nsiteweb").show();
            $("#nsiteweb").chained("#selectnweb");
            //$('#nsiteweb').window.location.reload():
            //window.reload():
        });
    });
});
</script>
<script>
function toggleDiv(divId) {
	$("#"+divId).toggle();
}
</script>
<?php
/* teste php*/

?>

<?php
$erreur=null;
$bdd = new PDO('mysql:host=127.0.0.1;dbname=getpass', 'root', '');
$reponsetbl = $bdd->prepare("SELECT * FROM types WHERE id_members='".$_SESSION['user']."'");
$reponsetbl->execute();
$donneestbl= $reponsetbl->fetchAll(PDO::FETCH_OBJ);

$reponseweb = $bdd->prepare("SELECT * FROM sites WHERE id_members=? ORDER BY id_types");
$reponseweb->execute(array($_SESSION['user']));
$donneesweb= $reponseweb->fetchAll(PDO::FETCH_OBJ);
//var_dump($donneesweb);
$selectnweb=0;
$lidweb=1;
//echo $selectnweb;

?>
<div class="indform">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>"  method="POST">
        <div class="row">
            <div class="col-sm-8 col-md-6 col-lg-2 offset-lg-5 offset-md-4">
                <select class="ajaxlist" name="selectnweb" id="selectnweb">
                    <option value="1">Sélectionner un type</option>
                    <option value="2">Email</option>
                    <option value="3">Boutique en ligne</option>
                    <option value="4">Réseaux sociaux</option>
                    <?php foreach ($donneestbl as $nweb):?>
                        <option value="<?php echo $nweb->id_types;?>"<?php echo $selectnweb == $nweb->id_types; ?>><?php echo $nweb->name_types;?></option> <!--condition facultative-->
                    <?php endforeach; ?>
                </select>
            </div>
            <!--<p>Nom du gateau : <input type="text" name="nom" id="nom"></p>-->
        </div>
        <div class="row">
            <div class="col-sm-8 col-md-6 col-lg-2 offset-lg-5 offset-md-4" >
                <select name="nsiteweb" id="nsiteweb">
                    <?php foreach ($donneesweb as $nweb):?>
                        <option value="<?php echo $nweb->id_sites;?>"  data-chained="<?php echo $nweb->id_sites;?>" class="<?php echo $nweb->id_types;?>"><?php echo $nweb->namesite;?></option> <!--condition facultative-->
                    <?php endforeach; ?>
                </select>
            </div>
        </div></br>
        <div class="indnew">
            <div class="row">
                <div class="col-sm-8 col-md-6 col-lg-2 offset-lg-5 offset-md-4">
                    <input class="subind" type="submit" name="ok"value="Rechercher">
                </div>
            </div></br>
        </div>
        <div class="indnew">
            <div class="row">
                <div class="col-sm-8 col-md-6 col-lg-2 offset-lg-5 offset-md-4">
                    <a href="new.php" class="newlmdp">Nouveau</a>
                </div>
            </div>
        </div></br>
    <?php
    if(isset($_POST['ok']) && !empty($_POST['ok']) && isset($_POST['nsiteweb']) && !empty($_POST['nsiteweb']) && isset($_POST['selectnweb']) && !empty($_POST['selectnweb'])){
        //echo "lol";
        $lidtypesweb=$_POST['selectnweb'];
        $lidweb=$_POST['nsiteweb'];
        $ready=sreachweb($lidweb); 
        $readytab=sreachtab($lidtypesweb);                                                                                
        $_SESSION['id_types']=$ready['id_types'];
        
        ?>
        
    <div class="tbl-reg">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post'>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
                    <p class="inp-reg">Enregistrer dans la table :</p>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2">
                    <p><?php echo $readytab['name_types'];?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
                    <p class="inp-reg">Nom d'enregistrement :</p>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-2" id="<?php echo $_SESSION['id_sites']=$ready['id_sites'];?>">
                    <p><?php echo $nom=$ready['namesite'];?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-1 offset-lg-4 offset-md-1 offset-sm-1">
                    <p class="inp-reg">Login :</p>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <input type="text" name="login" maxlength="250" value="<?php echo $ready['name_registration'];?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
                    <p class="inp-reg">Mot de passe :</p>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <input type="text" name="pass1" maxlength="64" value="<?php echo $ready['password_registration'];?>">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
                    <p class="inp-reg">Confirmez le mot de passe :</p>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <input type="text" name="pass2" maxlength="64" value="<?php echo $ready['password_registration'];?>">
                </div>
            </div>
            <div class="row" class="inp-mod">
                <div class="col-sm-1 col-md-1 col-lg-3 offset-lg-5 offset-md-5 offset-sm-1">
                    <input type="submit" name="update" value="Modifier !"><input type="submit" name="delete" value="Supprimer!"> 
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-7 col-md-7 col-lg-4 offset-lg-5 offset-md-5 offset-sm-1">
                <?php
                if(isset($_POST['update']) || isset($_POST['newlmdp'])){
                    echo $erreur;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    }
    // traitement des modifications
    if(isset($_POST['update']) && !empty($_POST['update'])){
        $lidweb=$_SESSION['id_sites']; 
        $login = htmlspecialchars($_POST['login']);
        $mdp1 =  htmlspecialchars($_POST['pass1']);
        $mdp2 =  htmlspecialchars($_POST['pass2']);
        $erreur=updatestock($lidweb, $login, $mdp1, $mdp2);
        
    }
    if (isset($_POST['delete']) && !empty($_POST['delete'])){
        $lidweb=$_SESSION['id_sites'];
        $lidtypesweb=$_SESSION['id_types'];
        //echo $lidweb;
        $erreur=verifdelstock($lidtypesweb);
        if($erreur==1){
        ?>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 offset-lg-3 offset-md-2">
                    <p class="inp-reg">Il existe qu'un seul site associé à ce type, vous également supprimer le type ?</p>
                </div>
                <div class="col-sm-1 col-md-1 col-lg-1 int-div-supp">
                    <input class="inp-sub" type="submit" name="cofdeletetype" value="Valider !">
                </div>
                <div class="col-sm-1 col-md-1 col-lg-1 int-div-supp">
                    <p>ou supprimer seulement le site</p><input class="inp-sub" type="submit" name="supdeletetype" value="Supprimer !">
                </div>
            </div>
        </form>  
    <?php
    } 
    } elseif($erreur==2){
        ?>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3 offset-lg-3 offset-md-2">
                        <p class="inp-reg">Etes vous sur de vouloir supprimer ce site?</p>
                    </div>
                    <div class="col-sm-1 col-md-1 col-lg-1 int-div-supp">
                        <input class="inp-sub" type="submit" name="confdelete" value="Valider !">
                    </div>
                    <div class="col-sm-1 col-md-1 col-lg-1 int-div-supp">
                        <input class="inp-sub" type="submit" name="" value="Annuler !">
                    </div>
                </div>
            </form>  
            <?php
            if(isset($_POST['confdelete'])){
                $erreur=deletestock($lidweb);
                echo $erreur;
            }
            ?>
    <?php
        }
        if(isset($_POST['cofdeletetype']) && !empty($_POST['cofdeletetype'])){
            $lidweb=$_SESSION['id_sites'];
            $lidtypesweb=$_SESSION['id_types'];
            $erreure=deletestock($lidweb);
            $erreur=deletestocktype($lidtypesweb);
        }
        if(isset($_POST['supdeletetype']) && !empty($_POST['supdeletetype'])){
            $lidweb=$_SESSION['id_sites'];
            $erreure=deletestock($lidweb);
        }
    ?>
</div>

<?php
require_once("includes/footer.php");
//echo $selectnweb;
?>
