<?php
require_once("includes/functions.php");
$user = detect_session(); // tester si l'utilisateur est identifié ?
require_once("includes/config.php");
include("includes/database.php");
require_once("includes/header.php");
?>
<meta http-equiv="refresh" content="30"/>
<div class="title-types">
    <div class="row">
        <div class="col-sm-2 col-md-2 col-lg-2 offset-md-5 offset-lg-5">
            <h2>Gérer les types</h2>
        </div>
    </div>
</div>

<?php
$lidtypesweb = null;
$bdd = new PDO('mysql:host=127.0.0.1;dbname=getpass', 'root', '');
$reponsetbl = $bdd->prepare("SELECT * FROM types WHERE id_members='".$_SESSION['user']."'");
$reponsetbl->execute();
$donneestbl= $reponsetbl->fetchAll(PDO::FETCH_OBJ);

?>
<?php
if(isset($_POST['name_types']) && !empty($_POST['name_types']) && isset($_POST['id_types']) && !empty($_POST['id_types']) && isset($_POST['delete']) && !empty($_POST['delete'])){
    $lidtypesweb=$_POST['id_types'];
    $erreur=deletestocktype($lidtypesweb);
}
if(isset($_POST['name_types']) && !empty($_POST['name_types']) && isset($_POST['id_types']) && !empty($_POST['id_types']) && isset($_POST['update']) && !empty($_POST['update'])){
    $lidtypesweb=$_POST['id_types'];
    $nametypesweb=$_POST['name_types'];
    $user=$_SESSION['user'];
    $erreur=updatestocktype($nametypesweb, $lidtypesweb, $user);
}
if(isset($_POST['new']) && !empty($_POST['new']) && isset($_POST['name_types']) && !empty($_POST['name_types'])){
    $nametypesweb=$_POST['name_types'];
    $erreur=ntable($nametypesweb);
}
?>

<div class="corp-type">
    <?php foreach ($donneestbl as $nweb):?>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>"  method="POST">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-2 offset-lg-5 offset-md-2">
                    <input type="texte" name="name_types" value="<?php echo $nweb->name_types;?>">
                </div>
                <div class="col-sm-2 col-md-2 col-lg-1">
                    <input type="submit" name="delete" value="Supprimer !">
                </div>
                <div class="col-sm-2 col-md-2 col-lg-1">
                    <input type="submit" name="update" onclick="setTimeout(timer, 5000)" value="Modifier !">
                </div>
                <input type="hidden" name="id_types" value="<?php echo $nweb->id_types;?>">
            </div>
        </form>
    <?php endforeach; ?>
    </br></br>
    <div class="row">
        <div class="col-sm-4 col-md-2 col-lg-2 offset-lg-5 offset-md-4 offset-sm-1">
            <a href="javascript:toggleDiv('nouvtbl');" class="newtbl">Nouveau Types</a>
        </div>
    </div></br>
    <div id="nouvtbl">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>"  method="POST">
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3 offset-lg-2 offset-md-2">
					<p class="inp-reg">Nom du nouveau type :</p>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-2">
					<input type="text" name="name_types" maxlength="250">
				</div>
			</div>
            <div class="row">
                <div class="col-sm-1 col-md-1 col-lg-1 offset-lg-5 offset-md-5 offset-sm-1">
                    <input class="inp-sub" type="submit" name="new" value="Enregistrez!">
                </div>
            </div>
        </form>
	</div>
        <div class="row">
            <div class="col-sm-7 col-md-7 col-lg-4 offset-lg-5 offset-md-5 offset-sm-1">
            <?php
            if(isset($_POST['name_types']) || isset($_POST['update'])){
                echo $erreur;
            }
            ?>
        </div>
    </div>
</div>


<script>

function toggleDiv(divId) {
	$("#"+divId).toggle();
	//$("#imgp").attr("src","css/images/moin.jpg");
}

</script>