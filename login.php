<?php require_once("includes/functions.php"); ?>
<?php $user = detect_session(1); // tester si l'utilisateur est identifié ?>
<?php include("includes/header.php"); ?>

<?php 
if(isset($_POST)){
	if(isset($_POST['log']) && !empty($_POST['login']) && !empty($_POST['pass'])) {
		//echo "formulaire envoyé avec succès";
		require_once("includes/config.php");
		include("includes/database.php");
		//$db = connect_database(); // connexion à la base de données
		$db = new PDO('mysql:host=127.0.0.1;dbname=getpass', 'root', '');
		// traitement pour test de l'identification
		$loginconnect = htmlspecialchars($_POST['login']);
	    $passconnect = sha1($_POST['pass']);
		if (!empty($loginconnect) && !empty($passconnect)) {
		   		# code...
			$requs= $db -> prepare ('SELECT * FROM members WHERE email=? AND mpassword=?');
			$requs -> execute (array($loginconnect, $passconnect));
			$userexiste = $requs -> rowCount();
				if ($userexiste == 1) {
					# code...
					$userinfo = $requs->fetch();
					$_SESSION['user']= $userinfo['id_members'];
					
					header("Location: index.php");
				} else {
					$erreur= "Email ou mot de passe incorrect.";
				}
		   	} else {
		   		$erreur = "Merci de renseigner votre adresse e-mail et votre mot de passe.";
		   	}
	} else {
		$erreur = "Merci de renseigner votre adresse e-mail et votre mot de passe.";
	}
	if(!empty($_POST['creat'])){
		header('Location: registration.php');
	}
}
?>

<div class="tbl-reg">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post'>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
				<p class="inp-reg">Adresse E-mail :</p>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-2">
				<input type="text" name="login" maxlength="250">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
				<p class="inp-reg">Mot de passe :</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2">
				<input type="password" name="pass" maxlength="250">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-md-10 col-lg-4 offset-lg-5 offset-md-1 offset-sm-1">
				<input id="log-reg" type="submit" name="log" value="Connexion"><input id="log-sub" type="submit" name="creat" value="Créer un compte">
			</div>
		</div>
		<div class="row">
			<?php
			if(isset($_POST['log'])){?>
				<div class="col-sm-10 col-md-10 col-lg-4 offset-lg-5 offset-md-1 offset-sm-1">
					<?php echo $erreur?>
				</div>
			<?php
			}
			?>
		</div>
	</form>
</div>


<?php require_once("includes/footer.php"); ?>
